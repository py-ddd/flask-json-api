import json
import unittest
from tests.app import app


class FlaskJsonTests(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def testHomePageShouldAllowNonApplicationJsonContentGivenHomePageWasAddedToExcludePaths(self):
        # given
        url = '/home'

        # when
        rv = self.app.get(url)

        # then
        self.assertEqual(rv.status_code, 200)
        self.assertIn('Home', rv.data.decode())

    def testValidJsonCall(self):
        # given
        url = '/api'
        headers = {'Content-Type': 'application/json'}

        # when
        rv = self.app.get(url, headers=headers)

        # then
        self.assertEqual(rv.status_code, 200)
        result = json.loads(rv.data)
        self.assertDictEqual(result, dict(message='ok'))

    def testJsonCallShouldFailWith406GivenContentTypeIsNotApplicationJson(self):
        # given
        url = '/api'
        headers = {'Content-Type': 'text/html'}

        # when
        rv = self.app.get(url, headers=headers)

        # then
        self.assertEqual(rv.status_code, 406)
        result = json.loads(rv.data)
        self.assertEqual(result['description'], 'Please specify application/json in the Content-Type header')

    def testJsonCallShouldFailWith415GivenNoContentTypeHeaderWasProvided(self):
        # given
        url = '/api'

        # when
        rv = self.app.get(url)

        # then
        self.assertEqual(rv.status_code, 415)
        result = json.loads(rv.data)
        self.assertEqual(result['description'], 'Please provide an application/json Content-Type header')

    def testJsonCallShouldFailWith500GivenGlobalError(self):
        # given
        url = '/api-with-global-error'

        # when
        rv = self.app.get(url, headers={'Content-Type': 'application/json'})

        # then
        self.assertEqual(rv.status_code, 500)
        result = json.loads(rv.data)
        self.assertEqual(result['description'],
                         'The server encountered an internal error and was unable to complete your request.  '
                         'Either the server is overloaded or there is an error in the application.')

    def testJsonCallShouldFailWith500GivenUnexpectedError(self):
        # given
        url = '/api-with-unexpected-error'

        # when
        rv = self.app.get(url, headers={'Content-Type': 'application/json'})

        # then
        self.assertEqual(rv.status_code, 500)
        result = json.loads(rv.data)
        self.assertEqual(result['description'], 'Unexpected error')

    def testJsonCallShouldFailWith400GivenExpectedError(self):
        # given
        url = '/api-with-expected-error'

        # when
        rv = self.app.get(url, headers={'Content-Type': 'application/json'})

        # then
        self.assertEqual(rv.status_code, 400)
        result = json.loads(rv.data)
        self.assertNotIn('description', result)
